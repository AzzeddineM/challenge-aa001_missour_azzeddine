package com.test.challengaa001.azzeddinemissour;

public class Rate {
    public String currency;
    public String value;
    public String icon;
    public String from;
    public String to;
    public String date;

    public Rate(){}

    public Rate(String currency, String value, String icon, String from, String to, String date) {
        this.currency = currency;
        this.value = value;
        this.icon = icon;
        this.from = from;
        this.to = to;
        this.date = date;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
