package com.test.challengaa001.azzeddinemissour;

public class DateRate {
    public float value;

    public DateRate(){}
    public DateRate(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
