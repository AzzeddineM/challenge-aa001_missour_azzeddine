package com.test.challengaa001.azzeddinemissour;

import java.util.ArrayList;
import java.util.Date;

public class Currency {
    public ArrayList<Rate> rates;
    public Date date;
    public String base;

    public Currency(){}

    public Currency(ArrayList<Rate> rates, Date date, String base) {
        this.rates = rates;
        this.date = date;
        this.base = base;
    }

    public ArrayList<Rate> getRates() {
        return rates;
    }

    public void setRates(ArrayList<Rate> rates) {
        this.rates = rates;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }
}
