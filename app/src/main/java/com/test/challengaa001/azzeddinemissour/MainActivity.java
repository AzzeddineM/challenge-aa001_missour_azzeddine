package com.test.challengaa001.azzeddinemissour;

import android.content.Context;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    RecyclerView recycleview;
    LinearLayout progressBar;
    String Json_Data;
    ProgressBar progress;
    TextView text,date,progressBarText;
    ArrayList<Rate> rat_list=new ArrayList<>(  );
    ArrayList<Currency> Currency_details = new ArrayList<Currency>();
    Spinner spin;
    int pos=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );


        //---------------------------initialise view------------------------------------------------
        //---------------------------initialise view------------------------------------------------
        //---------------------------initialise view------------------------------------------------
        spin = findViewById( R.id.spiner_currency );
        recycleview = this.findViewById( R.id.currency_exchenge );
        text = this.findViewById( R.id.text );
        date = this.findViewById( R.id.date );
        progressBarText = this.findViewById( R.id.progressBarText );
        progress = this.findViewById( R.id.progress );
        progressBar = this.findViewById( R.id.progressBar );

        //---------------------------call methods---------------------------------------------------
        //---------------------------call methods---------------------------------------------------
        //---------------------------call methods---------------------------------------------------

        check_cnx();





    }
    //---------------------------back press button---------------------------------------------------
    //---------------------------back press button---------------------------------------------------
    //---------------------------back press button---------------------------------------------------


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }


    //---------------------------check connection exist---------------------------------------------------
    //---------------------------check connection exist---------------------------------------------------
    //---------------------------check connection exist---------------------------------------------------


    public void check_cnx()
    {
        ConnectivityManager ConnectionManager=(ConnectivityManager)getSystemService( Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true )
        {

            String SAMPLE_URL="https://api.exchangeratesapi.io/latest";
            new TaskGetData().execute(SAMPLE_URL);
            progressBar.setVisibility( View.VISIBLE );
            recycleview.setVisibility( View.GONE );
        }
        else
        {
            progress.setVisibility( View.GONE );
            progressBar.setVisibility( View.VISIBLE );
            progressBarText.setVisibility( View.VISIBLE );
            recycleview.setVisibility( View.GONE );
            progressBarText.setText( "Please check your internet connection \n tap her to refresh" );
            progressBarText.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Animation anime= AnimationUtils.loadAnimation( MainActivity.this,R.anim.button_annimation_press);
                    view.startAnimation(anime);
                    anime.setAnimationListener( new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            check_cnx();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    } );

                }
            } );
        }
    }

    //---------------------------spiner adapter---------------------------------------------- ------
    //---------------------------spiner adapter---------------------------------------------- ------
    //-------------jnl--------------spiner adapter---------------------------------------------- ------

    public void spiner_adapter()
    {
        spin.getBackground().setColorFilter(getResources().getColor(android.R.color.black), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spiner_item, make_spinner(Currency_details));
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(this);
    }
    //jhdoezdjzo

    //---------------------------get value from  string Json methode and parse it to json data  ------
    //--------------------------- get value from string Json  methode and parse it to json data ------
    //--------------------------- get value from string Json  methode and parse it to json data ------



    public void json_currency()
    {


        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            JSONObject resObject = new JSONObject("{\"currency\": ["+Json_Data+"]}");
            JSONArray jsonArray = resObject.getJSONArray("currency");
            for (int i = 0; i < jsonArray.length(); i++) {


                JSONObject c = jsonArray.getJSONObject(i);
                Currency cur=new Currency();
                JSONObject resObjectrate = new JSONObject(c.getString( "rates" ));
                ArrayList<Rate> rate_list=new ArrayList<>(  );
                for (int t = 0; t < resObjectrate.length(); t++) {

                    Rate r=new Rate();
                    r.currency=resObjectrate.names().get( t ).toString();
                    r.value=resObjectrate.getString( resObjectrate.names().get( t ).toString());
                    rate_list.add( r );

                }

                cur.rates = rate_list;
                cur.date = formatter.parse( c.getString( "date" ) );
                date.setText( "Currency of : " + c.getString( "date" ) );
                cur.base = c.getString("base");
                Currency_details.add(cur);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        spiner_adapter();
        recycle_view_items(Currency_details);

        Log.i( "********* json_currency","good" );
    }



    //--------------------------- fill list with data for spiner -------------------------------
    //--------------------------- fill list with data for spiner -------------------------------
    //--------------------------- fill list with data for spiner -------------------------------

    public List<String> make_spinner(ArrayList<Currency> cur)
    {

        List<String> list = new ArrayList<>();
        for (int i=0; i<cur.size();i++)
        {

                list.add( cur.get( i ).base );

                for (int t=0;t<cur.get( i ).rates.size();t++)
                {

                    if(!cur.get( i ).base.equalsIgnoreCase( cur.get( i ).rates.get( t ).currency )) {
                        list.add( cur.get( i ).rates.get( t ).currency );
                    }
                }


        }
        Log.i( "********** spinList","good" );
        return  list;
    }



    //--------------------------- fill the recyclerView with data  -------------------------------
    //--------------------------- fill the recyclerView with data  -------------------------------
    //--------------------------- fill the recyclerView with data  -------------------------------


    public void recycle_view_items(ArrayList<Currency> currency)
    {


        for (int i=0; i<currency.size();i++)
        {

            for (int t=0;t<currency.get( i ).rates.size();t++)
            {

                if(!currency.get( i ).base.equalsIgnoreCase(currency.get( i ).rates.get( t ).currency  )) {
                    Rate rt = new Rate();
                    rt.currency = currency.get( i ).rates.get( t ).currency;
                    rt.value = currency.get( i ).rates.get( t ).value;
                    rt.icon ="https://fxtop.com/ico/"+currency.get( i ).rates.get( t ).currency+".gif";
                    rt.from = currency.get( i ).base;
                    rt.to   = currency.get( i ).rates.get( t ).currency;
                    rt.date = currency.get( i ).rates.get( t ).date;
                    rat_list.add( rt );
                }
            }

        }


        Currency_Adapter home_adapter = new Currency_Adapter(this, rat_list);
        recycleview.setAdapter(home_adapter);
        recycleview.setLayoutManager(new LinearLayoutManager(this));
        progressBar.setVisibility( View.GONE );
        recycleview.setVisibility( View.VISIBLE );
        Log.i( "******* recyclerView","good" );
    }



    //--------------------------- Open HttpUrlConnection -------------------------------
    //--------------------------- Open HttpUrlConnection -------------------------------
    //--------------------------- Open HttpUrlConnection -------------------------------

    public static String getDataHttpUriConnection(String uri){
        try {
            URL url = new URL(uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            String result = inputStreamToString(con.getInputStream());
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i( "********* httpUri","good" );
        return null;

    }

    //--------------------------- Get String from URL API -------------------------------
    //--------------------------- Get String from URL API -------------------------------
    //--------------------------- Get String from URL API -------------------------------

    public static String inputStreamToString(InputStream stream)  {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line = "";
        try
        {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line);
                sb.append("\n");
            }
            Log.i("**********************",sb.toString());
            return sb.toString();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        Log.i( "*********** streaming","good" );
        return null;

    }

    //--------------------------- Calling methode asynctask and getting data from API --------------
    //--------------------------- Calling methode asynctask and getting data from API --------------
    //--------------------------- Calling methode asynctask and getting data from API --------------


    public class TaskGetData extends AsyncTask<String , String , String>{


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            return getDataHttpUriConnection(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            rat_list.clear();
            Currency_details.clear();
            recycleview.removeAllViews();
            Json_Data=result;
            json_currency();
            Log.i( "**************** task","good" );

        }
    }

    //--------------------------- Fille  -------------------------------
    //--------------------------- Fille  -------------------------------
    //--------------------------- Fille  -------------------------------
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        int pos_item=position;
        pos_ver(pos_item);
        Log.i( "**************** spiner","good"+item );
        Log.i( "**************** spiner","good"+position );
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public void pos_ver(int pose)
    {
        if(pose!=pos)
        {
            Log.i( "**************** spiner",rat_list.get( pose ).currency+" *** "+pose );
            String SAMPLE_URL="https://api.exchangeratesapi.io/latest?base="+rat_list.get( pose-1 ).currency;
            new TaskGetData().execute(SAMPLE_URL);
        }
    }



}