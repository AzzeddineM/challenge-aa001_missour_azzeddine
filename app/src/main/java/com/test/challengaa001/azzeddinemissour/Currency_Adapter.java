package com.test.challengaa001.azzeddinemissour;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class Currency_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    List<Rate> data_url;
    Boolean vid_show = false;
    int cpt = 0;


    // create constructor to innitilize context and data sent from MainActivity
    public Currency_Adapter(Context context, List<Rate> data_url) {
        this.context = context;
        inflater = LayoutInflater.from( context );
        this.data_url = data_url;

    }

    // Inflate the layout when viewholder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate( R.layout.currency_adapter_items, parent, false );
        MyHolder holder = new MyHolder( view );
        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final MyHolder myHolder = (MyHolder) holder;
        myHolder.name.setText( data_url.get(position).currency);
        myHolder.value.setText( data_url.get(position).value);
        Glide.with(context).load(data_url.get( position ).icon).into(myHolder.flage);

        myHolder.layout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation anim = AnimationUtils.loadAnimation(context, R.anim.button_annimation_press );
                view.startAnimation(anim);
                anim.setAnimationListener( new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {


                        Intent history = new Intent(context,history.class);
                        history.putExtra("from",data_url.get(position).from);
                        history.putExtra("to",data_url.get(position).to);
                        history.putExtra("date",data_url.get(position).date);
                        context.startActivity(history);
                        ((Activity)context).finish();

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                } );
            }
        } );



    }


    @Override
    public int getItemCount() {
        return data_url.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {

        public ImageView flage;
        public TextView name;
        public TextView value;
        public RelativeLayout layout;

        public MyHolder(View itemView) {
            super( itemView );

            flage = (ImageView) itemView.findViewById( R.id.flage );
            name = (TextView) itemView.findViewById( R.id.currency_name );
            value = (TextView) itemView.findViewById( R.id.currency_value );
            layout = (RelativeLayout) itemView.findViewById( R.id.layout );

        }

    }
}