package com.test.challengaa001.azzeddinemissour;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;

public class history extends AppCompatActivity {
    String Json_Data;
    CircleImageView from,to;
    LineChart mchart;
    LinearLayout progressBar;
    TextView text,footertext;
    ArrayList<Entry> values = new ArrayList<>();
    final List list_x_axis_name = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_history );

        text=(TextView)this.findViewById( R.id.headtext );
        footertext=(TextView)this.findViewById( R.id.footertext );
        from=(CircleImageView) this.findViewById( R.id.flage_from );
        to=(CircleImageView)this.findViewById( R.id.flage_to );
        progressBar=(LinearLayout) this.findViewById( R.id.progressBar );
        mchart = (LineChart) findViewById(R.id.chart);


        Window window = getWindow();
        window.addFlags( WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        int colorCodeDark = Color.parseColor("#36cbf6");
        window.setStatusBarColor(colorCodeDark);
        main();

    }

    //---------------------------back press button---------------------------------------------------
    //---------------------------back press button---------------------------------------------------
    //---------------------------back press button---------------------------------------------------

    @Override
    public void onBackPressed() {
        Intent i=new Intent( this,MainActivity.class );
        startActivity( i );
        finish();
        super.onBackPressed();
    }

    //---------------------------main method---------------------------------------------------
    //---------------------------main method---------------------------------------------------
    //---------------------------main method---------------------------------------------------

    public void main()
    {
        Intent intent=getIntent();
        String mString=intent.getStringExtra("from")+" to "+intent.getStringExtra("to");
        text.setText( mString );

        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String currentDateandTime = date_format.format(new Date());


        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -2);
        Date date = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateof2monthago = format.format(date);

        String SAMPLE_URL="https://api.exchangeratesapi.io/history?start_at="
                +dateof2monthago+"&end_at="+currentDateandTime+"&symbols="+intent.getStringExtra("to")+"&base="+intent.getStringExtra("from");
        footertext.setText( "+ rates of last 60 days\n" + "+ Zoom the graph for more details" );
        text.setText("1"+intent.getStringExtra("from") +"    >>    "+intent.getStringExtra("to"));
        Glide.with(this).load("https://fxtop.com/ico/"+intent.getStringExtra("from")+".gif").into( from );
        Glide.with(this).load("https://fxtop.com/ico/"+intent.getStringExtra("to")+".gif").into(to);
        new TaskGetData().execute(SAMPLE_URL);
        mchart = findViewById(R.id.chart);
        mchart.setTouchEnabled(true);
        progressBar.setVisibility( View.VISIBLE );
        mchart.setVisibility( View.GONE );
    }
    //---------------------------graph config add values and show it to chart  ---------------------
    //---------------------------graph config add values and show it to chart  ---------------------
    //---------------------------graph config add values and show it to chart  ---------------------


    public String chart()
    {

        LineDataSet set1;
        if (mchart.getData() != null &&
            mchart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mchart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mchart.getData().notifyDataChanged();
            mchart.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(values, "Last 60 days ");
            set1.setDrawIcons(false);
            set1.enableDashedLine(1f, 5f, 0f);
            set1.enableDashedHighlightLine(1f, 5f, 0f);
            set1.setColor( Color.DKGRAY);
            set1.setCircleColor(Color.DKGRAY);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{1f, 5f}, 0f));
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_blue);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.DKGRAY);
            }
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);


            XAxis xAxis = mchart.getXAxis();
            xAxis.setGranularity(1f);
            xAxis.setCenterAxisLabels(true);
            xAxis.setLabelRotationAngle(-90);
            xAxis.setValueFormatter( new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    if (value >= 0) {
                    if (value <= list_x_axis_name.size() - 1) {
                      return (String) list_x_axis_name.get((int) value);
                    }return ""; }return "";
                }
            } );

            mchart.clear();
            LineData data = new LineData(dataSets);
            mchart.setData(data);
        }
        return "afficher";
    }

    //---------------------------get value from  string Json methode and parse it to json data  ------
    //--------------------------- get value from string Json  methode and parse it to json data ------
    //--------------------------- get value from string Json  methode and parse it to json data ------

    public void json_currency()
    {


        List<String> date=new ArrayList<>(  );
        Log.i( "in**********","value " );
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            JSONObject resObject = new JSONObject("{\"currency\": ["+Json_Data+"]}");
            JSONArray jsonArray = resObject.getJSONArray("currency");
            for (int i = 0; i < jsonArray.length(); i++) {

                Log.i( "good aaaaa "+i,"value "+i );

                JSONObject c = jsonArray.getJSONObject(i);
                JSONObject resObjectrate = new JSONObject(c.getString( "rates" ));
                for (int t = 0; t < resObjectrate.length(); t++) {
                    date.add( resObjectrate.names().get( t ).toString()  );

                    Log.i( "good tttttt ", "date :" + resObjectrate.names().get( t ).toString() );

                }
                JsonDate(date,resObjectrate);




            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.i( "******** json_currency","good" );

    }

    //---------------------------get value from  string Json methode and parse it to json data  ------
    //--------------------------- get value from string Json  methode and parse it to json data ------
    //--------------------------- get value from string Json  methode and parse it to json data ------

    public void JsonDate(List<String> date,JSONObject jsonObject)
    {


        try {
            JSONObject resObject = new JSONObject("{\"DateRate\": ["+jsonObject.toString()+"]}");
            JSONArray jsonArray = resObject.getJSONArray("DateRate");


            Log.i( "**************** lenght", ""+jsonArray.getString( 0 ).indexOf( 1 ) );


            for (int i = 0; i < jsonArray.length(); i++) {


                JSONObject c = jsonArray.getJSONObject( i );

                for (int ctp=0;ctp<date.size();ctp++) {

                    JSONObject resObjectrate = new JSONObject( c.getString( date.get( ctp ) ) );

                    for (int t = 0; t < resObjectrate.length(); t++) {

                        Log.i( "**************** date", "" + date.get( ctp ));
                        Log.i( "*********** Currencyt", "" + resObjectrate.names().get( t ).toString() );
                        Log.i( "**************** Rate", "" +Double.parseDouble( resObjectrate.getString( resObjectrate.names().get( t ).toString()) ));

                        values.add(new Entry(ctp,Float.parseFloat( resObjectrate.getString( resObjectrate.names().get( t ).toString()))));
                        list_x_axis_name.add(date.get( ctp ));

                        progressBar.setVisibility( View.GONE );
                        mchart.setVisibility( View.VISIBLE );
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        chart();

    }

    //--------------------------- Open HttpUrlConnection -------------------------------
    //--------------------------- Open HttpUrlConnection -------------------------------
    //--------------------------- Open HttpUrlConnection -------------------------------

    public static String getDataHttpUriConnection(String uri){
        try {
            URL url = new URL(uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            String result = inputStreamToString(con.getInputStream());
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i( "***** history httpUri","good" );
        return null;

    }

    //--------------------------- Get String from URL API -------------------------------
    //--------------------------- Get String from URL API -------------------------------
    //--------------------------- Get String from URL API -------------------------------

    public static String inputStreamToString(InputStream stream)  {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line = "";
        try
        {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line);
                sb.append("\n");
            }
            Log.i("************ value",sb.toString());
            return sb.toString();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        Log.i( "******history streaming","good" );
        return null;

    }




    public class TaskGetData extends AsyncTask<String , String , String> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {
            return getDataHttpUriConnection(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            Json_Data=result;
            json_currency();

            //json_currency();
            Log.i( "*********history task","good" );

        }
    }

}